package com.example.anhquoc.ukvdemo.Aid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.MainActivity;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 2/13/2017.
 */
public class AidFragment extends Fragment {
    private ImageButton mBackToAid;
    private Button mGenealBtn;
    private Button mSpecificBtn;
    private Button mApplicationBtn;
    private Button mBillBtn;
    private Button mPKVGeneralBtn;
    private Button mPKVSpecificBtn;
    private Button mPKVApplicationBtn;
    private Button mPKVBillBtn;
    private Button mChatBtn;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.aid_fragment, container, false);
        mBackToAid = (ImageButton) view.findViewById(R.id.navigation_help_back_img);
        mGenealBtn = (Button) view.findViewById(R.id.general_btn);
        mSpecificBtn = (Button) view.findViewById(R.id.specific_btn);
        mApplicationBtn = (Button) view.findViewById(R.id.application_btn);
        mBillBtn = (Button) view.findViewById(R.id.bill_btn);
        mPKVGeneralBtn = (Button) view.findViewById(R.id.pkv_general_btn);
        mPKVSpecificBtn = (Button)view.findViewById(R.id.pkv_specific_btn);
        mPKVApplicationBtn = (Button) view.findViewById(R.id.pkv_application_btn);
        mPKVBillBtn = (Button) view.findViewById(R.id.pkv_bill_btn);
        mChatBtn = (Button) view.findViewById(R.id.chat_btn);

        mBackToAid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).openDrawerFromFragment();
            }
        });
        mGenealBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AidInfoFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });
        mSpecificBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SaarlandInfoFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });
        mApplicationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Beihilfe Beantragen");
                Fragment fragment = new NotDoneAidFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });
        mBillBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new BillFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });



        mPKVGeneralBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Allgemein");
                Fragment fragment = new NotDoneAidFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mPKVSpecificBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Spezifisch");
                Fragment fragment = new NotDoneAidFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mPKVApplicationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "PKV Beantragen");
                Fragment fragment = new NotDoneAidFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mPKVBillBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Rechnung Einreichen");
                Fragment fragment = new NotDoneAidFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mChatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setUserChatFragment();
            }
        });
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
