package com.example.anhquoc.ukvdemo.Aid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 4/17/2017.
 */
public class AidInfoFragment extends Fragment {
    private ImageButton mBackImgBtn;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.aid_info_fragment, container, false);
        mBackImgBtn = (ImageButton) view.findViewById(R.id.aid_info_back_img);
        mBackImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AidFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        return view;
    }
}
