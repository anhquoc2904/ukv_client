package com.example.anhquoc.ukvdemo.Aid;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/19/2016.
 */
public class BillFragment extends Fragment {
    ImageButton mBackToNavigationHelpBtn;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.bill, container, false);
        mBackToNavigationHelpBtn = (ImageButton) view.findViewById(R.id.bill_back_img);
        mBackToNavigationHelpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AidFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });
        return view;
    }
}
