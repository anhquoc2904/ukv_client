package com.example.anhquoc.ukvdemo.Aid;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/19/2016.
 */
public class ChildInsureActivity extends Activity {
    ImageButton mBackToNavigationHelpBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.child_insure);
        mBackToNavigationHelpBtn = (ImageButton) findViewById(R.id.child_back_img);
        mBackToNavigationHelpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
