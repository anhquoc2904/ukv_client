package com.example.anhquoc.ukvdemo.Aid;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/19/2016.
 */
public class MarriedInsureActivity extends Activity{
    ImageButton mBackToNavigationHelpBtn;
    ImageView mMarriedInsureType;
    ImageView mPrivateImg;
    ImageView mLegalImg;
    ImageView mQuotaImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.married_insure);
        mBackToNavigationHelpBtn = (ImageButton) findViewById(R.id.married_back_img);
        mMarriedInsureType = (ImageView) findViewById(R.id.married_1_img) ;
        mPrivateImg = (ImageView) findViewById(R.id.married_2_img) ;
        mLegalImg = (ImageView) findViewById(R.id.married_3_img) ;
        mQuotaImg = (ImageView) findViewById(R.id.married_4_img) ;
        mBackToNavigationHelpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mQuotaImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mQuotaImg.setVisibility(View.GONE);
                mPrivateImg.setClickable(true);
                mMarriedInsureType.setImageResource(R.drawable.married_type_change);
                mPrivateImg.setImageResource(R.drawable.private_change);
                mLegalImg.setImageResource(R.drawable.legal_change);
            }
        });

        mPrivateImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPrivateImg.setClickable(false);
                mPrivateImg.setVisibility(View.GONE);
                mLegalImg.setVisibility(View.GONE);
                mQuotaImg.setVisibility(View.GONE);
                mMarriedInsureType.setImageResource(R.drawable.legal_chang_second_time);
            }
        });
    }
}
