package com.example.anhquoc.ukvdemo.Aid;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.Apply.CheckPoliceActivity;
import com.example.anhquoc.ukvdemo.MainActivity;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/19/2016.
 */
public class NavigationHelpFragment extends Fragment {
    private ImageButton mBackToNavigationHelp;
    private Button mSaarlandInfoBtn;
    private Button mBillBtn;
    private Button mChildInsureBtn;
    private Button mMarriedInsureBtn;
    private Button mPoliceCheckBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.navigation_help, container, false);
        mBackToNavigationHelp = (ImageButton) view.findViewById(R.id.navigation_help_back_img);
        mSaarlandInfoBtn = (Button) view.findViewById(R.id.saarland_information_btn);
        mBillBtn = (Button) view.findViewById(R.id.bill_btn);
        mChildInsureBtn = (Button) view.findViewById(R.id.child_insure);
        mMarriedInsureBtn = (Button)view.findViewById(R.id.married_insure);
        mPoliceCheckBtn = (Button) view.findViewById(R.id.police_btn);

        mBackToNavigationHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).openDrawerFromFragment();
            }
        });
        mSaarlandInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SaarlandInfoFragment.class);
                startActivity(intent);
            }
        });

        mBillBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), BillFragment.class);
                startActivity(intent);
            }
        });



        mChildInsureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ChildInsureActivity.class);
                startActivity(intent);
            }
        });

        mMarriedInsureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MarriedInsureActivity.class);
                startActivity(intent);
            }
        });

        mPoliceCheckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CheckPoliceActivity.class);
                startActivity(intent);
            }
        });
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
