package com.example.anhquoc.ukvdemo.Aid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.anhquoc.ukvdemo.MainActivity;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/14/2016.
 */
public class NavigationalHelpFilterFragment extends Fragment {
    private ImageButton mBackToDrawerImgBtn;
    private Button mSearchBtn;
    private Button mDoneCheckBtn;
    private ImageView mPoliceImg;
    private ImageView mTeacherImg;
    private ImageView mFiremanImg;
    private ImageView mLawyerImg;
    private ImageView mMarriageImg;
    private ImageView mSingleImg;
    private ImageView mFamilyImg;
    private boolean mPoliceChecked = false;
    private boolean mTeacherChecked = false;
    private boolean mFiremanChecked = false;
    private boolean mLawyerChecked = false;
    private boolean mMarriageChecked = false;
    private boolean mSingleChecked = false;
    private boolean mFamilyChecked = false;
    private Spinner mJobsSpinner;
    private LinearLayout mCheckboxLayout;
    private LinearLayout mMapsJobsLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.navigational_help_filter_fragment, container, false);
        mBackToDrawerImgBtn = (ImageButton) view.findViewById(R.id.navigation_help_back_img);
        mSearchBtn = (Button) view.findViewById(R.id.search_btn);
        mJobsSpinner = (Spinner) view.findViewById(R.id.jobs_spinner);
        mCheckboxLayout = (LinearLayout) view.findViewById(R.id.filter_checkbox_layout);
        mMapsJobsLayout = (LinearLayout) view.findViewById(R.id.maps_job_filter_layout);
        mDoneCheckBtn = (Button) view.findViewById(R.id.done_check_btn);
        mPoliceImg = (ImageView) view.findViewById(R.id.police_filter);
        mTeacherImg = (ImageView) view.findViewById(R.id.teacher_filter);
        mFiremanImg = (ImageView) view.findViewById(R.id.fireman_filter);
        mLawyerImg = (ImageView) view.findViewById(R.id.lawer_filter);
        mMarriageImg = (ImageView) view.findViewById(R.id.marriage_filter);
        mSingleImg = (ImageView) view.findViewById(R.id.single_filter);
        mFamilyImg = (ImageView) view.findViewById(R.id.family_filter);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.jobs_array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // Apply the adapter to the spinner
        mJobsSpinner.setAdapter(adapter);

        mJobsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mMapsJobsLayout.setVisibility(View.INVISIBLE);
                if (position == 0) {
                    mCheckboxLayout.setVisibility(View.INVISIBLE);
                } else {
                    mCheckboxLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mBackToDrawerImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).openDrawerFromFragment();
            }
        });



        mSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new NavigationHelpFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mDoneCheckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMapsJobsLayout.setVisibility(View.VISIBLE);
            }
        });

        mPoliceImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mPoliceChecked) {
                    mPoliceChecked = true;
                    mPoliceImg.setImageResource(R.drawable.police_checked);
                } else {
                    mPoliceChecked = false;
                    mPoliceImg.setImageResource(R.drawable.police);
                }
            }
        });

        mTeacherImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mTeacherChecked) {
                    mTeacherChecked = true;
                    mTeacherImg.setImageResource(R.drawable.teacher_checked);
                } else {
                    mTeacherChecked = false;
                    mTeacherImg.setImageResource(R.drawable.teacher);
                }
            }
        });

        mFiremanImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mFiremanChecked) {
                    mFiremanChecked = true;
                    mFiremanImg.setImageResource(R.drawable.fireman_checked);
                } else {
                    mFiremanChecked = false;
                    mFiremanImg.setImageResource(R.drawable.fireman);
                }
            }
        });

        mLawyerImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mLawyerChecked) {
                    mLawyerChecked = true;
                    mLawyerImg.setImageResource(R.drawable.lawer_checked);
                } else {
                    mLawyerChecked = false;
                    mLawyerImg.setImageResource(R.drawable.lawer);
                }
            }
        });

        mMarriageImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mMarriageChecked) {
                    mMarriageChecked = true;
                    mMarriageImg.setImageResource(R.drawable.marriage_checked);
                } else {
                    mMarriageChecked = false;
                    mMarriageImg.setImageResource(R.drawable.marriage);
                }
            }
        });

        mSingleImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSingleChecked) {
                    mSingleChecked = true;
                    mSingleImg.setImageResource(R.drawable.single_checked);
                } else {
                    mSingleChecked = false;
                    mSingleImg.setImageResource(R.drawable.single);
                }
            }
        });

        mFamilyImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mFamilyChecked) {
                    mFamilyChecked = true;
                    mFamilyImg.setImageResource(R.drawable.family_checked);
                } else {
                    mFamilyChecked = false;
                    mFamilyImg.setImageResource(R.drawable.family);
                }
            }
        });
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
