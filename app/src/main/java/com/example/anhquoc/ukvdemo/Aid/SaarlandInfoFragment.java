package com.example.anhquoc.ukvdemo.Aid;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.anhquoc.ukvdemo.OnSwipeTouchListener;
import com.example.anhquoc.ukvdemo.R;
import com.example.anhquoc.ukvdemo.Setting.ProfileFragment;
import com.example.anhquoc.ukvdemo.Setting.SettingFragment;

/**
 * Created by Anh Quoc on 10/19/2016.
 */
public class SaarlandInfoFragment extends Fragment {
    private ImageButton mBackToNavigationHelpImgBtn;
    private ImageButton mNextSaarlandImgBtn;
    private ImageView mSaarlandImage;
    private int mSaarlandImageNumber = 1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.saarland, container, false);
        mBackToNavigationHelpImgBtn = (ImageButton) view.findViewById(R.id.saarland_back_img);
        mNextSaarlandImgBtn = (ImageButton) view.findViewById(R.id.saarland_next_img);
        mSaarlandImage = (ImageView) view.findViewById(R.id.saarland_img);

        mBackToNavigationHelpImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSaarlandImageNumber == 1) {
                    Fragment fragment = new AidFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, fragment)
                            .commit();
                }
                else if (mSaarlandImageNumber == 2) {
                    mSaarlandImageNumber--;
                    mNextSaarlandImgBtn.setVisibility(View.VISIBLE);
                    mSaarlandImage.setImageResource(R.drawable.saarland_1);
                }
            }
        });

        mNextSaarlandImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSaarlandImageNumber++;
                mNextSaarlandImgBtn.setVisibility(View.INVISIBLE);
                mSaarlandImage.setImageResource(R.drawable.saarland_2);
            }
        });

        mSaarlandImage.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {
                if (mSaarlandImageNumber == 1) {
                    Fragment fragment = new AidFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, fragment)
                            .commit();
                }
                else if (mSaarlandImageNumber == 2) {
                    mSaarlandImageNumber --;
                    mNextSaarlandImgBtn.setVisibility(View.VISIBLE);
                    mSaarlandImage.setImageResource(R.drawable.saarland_1);
                }
            }
            public void onSwipeLeft() {
                if (mSaarlandImageNumber<2) {
                    mSaarlandImageNumber++;
                }
                mNextSaarlandImgBtn.setVisibility(View.INVISIBLE);
                mSaarlandImage.setImageResource(R.drawable.saarland_2);
            }
            public void onSwipeBottom() {
            }

        });

        return view;
    }
}
