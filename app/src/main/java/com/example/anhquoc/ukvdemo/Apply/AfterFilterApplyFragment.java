package com.example.anhquoc.ukvdemo.Apply;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.MainActivity;
import com.example.anhquoc.ukvdemo.R;
import com.example.anhquoc.ukvdemo.Setting.NotDoneFragment;

/**
 * Created by Anh Quoc on 2/13/2017.
 */
public class AfterFilterApplyFragment extends Fragment {
    private ImageButton mBackToApply;
    private Button mRequirementBtn;
    private Button mFormBtn;
    private Button mHowBtn;
    private Button mReminderBtn;
    private Button mToDoBtn;
    private Button mPoliceCheckBtn;
    private Button mWalletCheckBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.apply_after_filter, container, false);
        mBackToApply = (ImageButton) view.findViewById(R.id.navigation_help_back_img);
        mRequirementBtn = (Button) view.findViewById(R.id.requirement_btn);
        mFormBtn = (Button) view.findViewById(R.id.form_btn);
        mHowBtn = (Button) view.findViewById(R.id.how_btn);
        mReminderBtn = (Button)view.findViewById(R.id.reminder_btn);
        mToDoBtn = (Button) view.findViewById(R.id.todo_btn);
        mPoliceCheckBtn = (Button) view.findViewById(R.id.checklist_btn);
        mWalletCheckBtn = (Button) view.findViewById(R.id.wallet_check_btn);

        mBackToApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).openDrawerFromFragment();
            }
        });
        mRequirementBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Voraussetzungen");
                Fragment fragment = new NotDoneApplyActivity();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();

            }
        });

        mFormBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Bewerbe Formalien");
                Fragment fragment = new NotDoneApplyActivity();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });



        mHowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Wie Bewerbung einreichen");
                Fragment fragment = new NotDoneApplyActivity();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();

            }
        });

        mReminderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new CalendarFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mToDoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Meine ToDos");
                Fragment fragment = new NotDoneApplyActivity();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();

            }
        });
        mPoliceCheckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new CheckPoliceActivity();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mWalletCheckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Bewerbungsmappen-Check");
                Fragment fragment = new NotDoneApplyActivity();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();

            }
        });
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
