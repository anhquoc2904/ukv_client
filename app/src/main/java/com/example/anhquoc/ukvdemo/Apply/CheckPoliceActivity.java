package com.example.anhquoc.ukvdemo.Apply;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.anhquoc.ukvdemo.OnSwipeTouchListener;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/19/2016.
 */
public class CheckPoliceActivity extends Fragment {
    private ImageButton mBackToNavigationHelpImgBtn;
    private ImageButton mNextPoliceCheckImgBtn;
    private ImageView mPoliceCheckImage;
    private int mPoliceImageNumber = 1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.check_police, container, false);
        mBackToNavigationHelpImgBtn = (ImageButton) view.findViewById(R.id.check_police_back_img);
        mNextPoliceCheckImgBtn = (ImageButton) view.findViewById(R.id.check_police_next_img);
        mPoliceCheckImage = (ImageView) view.findViewById(R.id.police_img);

        mBackToNavigationHelpImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPoliceImageNumber == 1) {
                    Fragment fragment = new AfterFilterApplyFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, fragment)
                            .commit();
                }
                else if (mPoliceImageNumber == 2) {
                    mPoliceImageNumber--;
                    mNextPoliceCheckImgBtn.setVisibility(View.VISIBLE);
                    mPoliceCheckImage.setImageResource(R.drawable.police_check_1);
                }
            }
        });

        mNextPoliceCheckImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPoliceImageNumber++;
                mNextPoliceCheckImgBtn.setVisibility(View.INVISIBLE);
                mPoliceCheckImage.setImageResource(R.drawable.police_check_2);
            }
        });

        mPoliceCheckImage.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {
                if (mPoliceImageNumber == 1) {
                    Fragment fragment = new AfterFilterApplyFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, fragment)
                            .commit();
                }
                else if (mPoliceImageNumber == 2) {
                    mPoliceImageNumber --;
                    mNextPoliceCheckImgBtn.setVisibility(View.VISIBLE);
                    mPoliceCheckImage.setImageResource(R.drawable.police_check_1);
                }
            }
            public void onSwipeLeft() {
                if (mPoliceImageNumber<2) {
                    mPoliceImageNumber++;
                }
                mNextPoliceCheckImgBtn.setVisibility(View.INVISIBLE);
                mPoliceCheckImage.setImageResource(R.drawable.police_check_2);
            }
            public void onSwipeBottom() {
            }

        });
        return view;
    }
}
