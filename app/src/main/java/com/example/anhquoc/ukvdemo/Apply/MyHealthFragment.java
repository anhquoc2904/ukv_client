package com.example.anhquoc.ukvdemo.Apply;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.MainActivity;
import com.example.anhquoc.ukvdemo.MapsActivity;
import com.example.anhquoc.ukvdemo.R;
import com.example.anhquoc.ukvdemo.Setting.EmergencyInfoFragment;
import com.example.anhquoc.ukvdemo.Sport.NutritionTipsFragment;

/**
 * Created by Anh Quoc on 10/14/2016.
 */
public class MyHealthFragment extends Fragment {

    private Button mEmergencyInformationBtn;
    private Button mHealthMapBtn;
    private Button mNutritionTipsBtn;
    private Button mUserChatBtn;
    private ImageButton mBackToDrawerImgBtn;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.myhealth_fragment, container, false);
        mEmergencyInformationBtn = (Button) view.findViewById(R.id.emergency_information_btn);
        mHealthMapBtn = (Button) view.findViewById(R.id.health_map_btn);
        mNutritionTipsBtn = (Button) view.findViewById(R.id.nutrition_tips_btn);
        mUserChatBtn = (Button) view.findViewById(R.id.film_btn);
        mBackToDrawerImgBtn = (ImageButton) view.findViewById(R.id.my_health_back_img);

        mEmergencyInformationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EmergencyInfoFragment.class);
                startActivity(intent);
            }
        });

        mHealthMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                startActivity(intent);
            }
        });


        mNutritionTipsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NutritionTipsFragment.class);
                startActivity(intent);
            }
        });
        mUserChatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).setUserChatFragment();
            }
        });
        mBackToDrawerImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).openDrawerFromFragment();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
