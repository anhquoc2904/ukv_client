package com.example.anhquoc.ukvdemo;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 12/4/2016.
 */
public class CustomDialogClass extends Dialog {
    public Activity mActivity;
    public Dialog mDialog;
    public Button mButtonBack;
    public int mGuideNumber = 0;
    public CustomDialogClass(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.mActivity = a;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.air_pollution_status_popup);

        mButtonBack = (Button) findViewById(R.id.back);
        mButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
