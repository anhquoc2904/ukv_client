package com.example.anhquoc.ukvdemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.anhquoc.ukvdemo.Setting.SettingFragment;
import com.example.anhquoc.ukvdemo.Aid.AidFragment;
import com.example.anhquoc.ukvdemo.Apply.FilterFragment;
import com.example.anhquoc.ukvdemo.Network.CommunityFragment;
import com.example.anhquoc.ukvdemo.Network.UserChatFragment;
import com.example.anhquoc.ukvdemo.Sport.SportFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
        Toolbar toolbar;
        Activity mActivity;
        NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mActivity = this;
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        setProfileInDrawerFromSharedPreferences();

        Fragment mFragment = new WelcomeFragment();
        setFragment(mFragment);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment mFragment = null;
        int id = item.getItemId();

        if (id == R.id.setting) {
            toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.my_ukv_color)));
            mFragment = new SettingFragment();
        } else if (id == R.id.apply) {
            toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.apply_color)));
            Bundle bundle=new Bundle();
            bundle.putString("CallerFragment", "Apply");
            mFragment = new FilterFragment();
            mFragment.setArguments(bundle);
        } else if (id == R.id.aid) {
//            toolbar.setBackgroundColor(getResources().getColor(R.color.navigation_color));
//            mFragment = new AidFragment();
            toolbar.setBackgroundColor(getResources().getColor(R.color.navigation_color));
            Bundle bundle=new Bundle();
            bundle.putString("CallerFragment", "Aid");
            mFragment = new FilterFragment();
            mFragment.setArguments(bundle);
        } else if (id == R.id.sport) {
            toolbar.setBackgroundColor(getResources().getColor(R.color.ukv_color));
            mFragment = new SportFragment();
//        } else if (id == R.id.expert_chat) {
//            toolbar.setBackground(new ColorDrawable(getResources().getColor(R.color.ukv_color)));
//            mFragment = new WelcomeFragment();
        } else if (id == R.id.community) {
            toolbar.setBackgroundColor(getResources().getColor(R.color.my_ukv_color));
            mFragment = new CommunityFragment();
        }
        if (mFragment != null) {
            setFragment(mFragment);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setFragment(Fragment mFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_layout, mFragment)
                .commit();
    }

    public void setUserChatFragment() {
        toolbar.setBackgroundColor(getResources().getColor(R.color.my_ukv_color));
        Fragment mFragment = new UserChatFragment();
        setFragment(mFragment);
    }
    public void openDrawerFromFragment(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (!drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.openDrawer(GravityCompat.START);
        }
    }
    public void setProfileInDrawerFromSharedPreferences() {

        SharedPreferences sharedPref = mActivity.getSharedPreferences("UserProfile", Context.MODE_PRIVATE);

        Menu menu = navigationView.getMenu();
        MenuItem userJob = menu.findItem(R.id.user_job);
        userJob.setTitle(sharedPref.getString("User Job", "Unknown"));

        MenuItem userFamilyStatus = menu.findItem(R.id.user_family_status);
        userFamilyStatus.setTitle(sharedPref.getString("User Family Status", "Unknown"));

        MenuItem userState = menu.findItem(R.id.user_state);
        userState.setTitle(sharedPref.getString("User State", "Unknown"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
