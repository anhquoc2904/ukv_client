package com.example.anhquoc.ukvdemo.Network;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/18/2016.
 */
public class CustomerChatActivity extends Activity {
    private ImageButton mBackToUserChatImgBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_chat);

        mBackToUserChatImgBtn = (ImageButton) findViewById(R.id.customer_chat_back_img);
        mBackToUserChatImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
