package com.example.anhquoc.ukvdemo.Network;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.example.anhquoc.ukvdemo.R;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by Anh Quoc on 1/6/2017.
 */
public class GroupChatActivity extends Activity {
    private static final String TAG = "ChatActivity";
    private Context mContext;
    private GroupChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText chatText;
    private Button buttonSend;
    private boolean side = false;
    private ImageButton mBackToUserChatImgBtn;

    private Client mClientUser;
    private InetAddress mServerUser;
    private String mServerUserIPAddress;
    private String mDeviceName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mDeviceName = Build.MODEL;
        setContentView(R.layout.network_chat_group);
        mBackToUserChatImgBtn = (ImageButton) findViewById(R.id.customer_chat_back_img);
        mBackToUserChatImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        buttonSend = (Button) findViewById(R.id.send);

        listView = (ListView) findViewById(R.id.msgview);

        chatArrayAdapter = new GroupChatArrayAdapter(getApplicationContext(), R.layout.right);
        listView.setAdapter(chatArrayAdapter);

        chatText = (EditText) findViewById(R.id.msg);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return sendChatMessage();
                }
                return false;
            }
        });
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (!mClientUser.isConnected()) {
                    mClientUser.close();
                    setUpConnectionToServer();
                    Toast toast = Toast.makeText(mContext, "Can not connect to server. Try it again", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    sendChatMessage();
                }
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);

        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });

        setUpConnectionToServer();
    }

    private boolean sendChatMessage() {
        final GroupChatMessage mChatMessage = new GroupChatMessage();
        mChatMessage.left = false;
        mChatMessage.name = mDeviceName;
        mChatMessage.message = chatText.getText().toString();
//        chatArrayAdapter.add(mChatMessage);
        chatText.setText("");
        new Thread(new Runnable() {
            @Override
            public void run() {
                mClientUser.sendTCP(mChatMessage);
                Log.d(TAG, "Send data to doctor " + mChatMessage.message);
            }
        }).start();
        return true;
    }

    private boolean receiveChatMessage(GroupChatMessage receivedMessage) {
        Log.d(TAG, receivedMessage.message);
        Log.d(TAG, Boolean.toString(receivedMessage.left));
        receivedMessage.left = false;
        chatArrayAdapter.add(receivedMessage);
        return true;
    }

    private void setUpConnectionToServer() {
        mClientUser = new Client();
        mClientUser.start();

        Log.d(TAG, "Setup server connection");
        new Thread("Connect") {
            public void run() {

                mServerUser = mClientUser.discoverHost(54427, 5000);

                if (mServerUser != null) {
                    Log.d(TAG, mServerUser.getHostAddress());
                    mServerUserIPAddress = mServerUser.getHostAddress();
                    try {
                        mClientUser.connect(5000, mServerUserIPAddress, 54425, 54427);
                        ConnectRequestMessage mConnectRequestMessage = new ConnectRequestMessage();
                        mConnectRequestMessage.message = "connect request";
                        mClientUser.sendTCP(mConnectRequestMessage);
                        // Server communication after connection can go here, or in Listener#connected().
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        System.exit(1);
                    }
                }
            }
        }.start();
        mClientUser.addListener(new Listener() {
            public void received(Connection connection, Object object) {
                if (object instanceof GroupChatMessage) {
                    Log.d(TAG, "Receive an ChatMessage object  ");
                    final GroupChatMessage mReceiveMessage = (GroupChatMessage) object;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            receiveChatMessage(mReceiveMessage);
                        }
                    });
                }
            }
        });
        Kryo kryo = mClientUser.getKryo();
        kryo.register(ConnectRequestMessage.class);
        kryo.register(GroupChatMessage.class);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mClientUser.isConnected()) {
            mClientUser.close();
        }
    }
}
