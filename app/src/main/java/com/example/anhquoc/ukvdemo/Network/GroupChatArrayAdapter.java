package com.example.anhquoc.ukvdemo.Network;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.anhquoc.ukvdemo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anh Quoc on 1/6/2017.
 */
public class GroupChatArrayAdapter extends ArrayAdapter<GroupChatMessage> {
    private TextView chatText;
    private TextView deviceName;
    private List<GroupChatMessage> chatMessageList = new ArrayList<>();
    private Context context;

    @Override
    public void add(GroupChatMessage object) {
        chatMessageList.add(object);
        super.add(object);
    }

    public GroupChatArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public int getCount() {
        return this.chatMessageList.size();
    }

    public GroupChatMessage getItem(int index) {
        return this.chatMessageList.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        GroupChatMessage chatMessageObj = getItem(position);
        View row = convertView;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (chatMessageObj.left) {
            row = inflater.inflate(R.layout.right, parent, false);
        }else{
            row = inflater.inflate(R.layout.group_left, parent, false);
        }
        chatText = (TextView) row.findViewById(R.id.msgr);
        deviceName = (TextView) row.findViewById(R.id.device_name);
        chatText.setText(chatMessageObj.message);
        deviceName.setText(chatMessageObj.name);
        return row;
    }
}
