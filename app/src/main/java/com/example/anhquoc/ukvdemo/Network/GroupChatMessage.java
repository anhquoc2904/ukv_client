package com.example.anhquoc.ukvdemo.Network;

/**
 * Created by Anh Quoc on 1/6/2017.
 */
public class GroupChatMessage {
    public boolean left;
    public String message;
    public String name;
}
