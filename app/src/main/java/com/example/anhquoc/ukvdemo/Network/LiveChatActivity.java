package com.example.anhquoc.ukvdemo.Network;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/17/2016.
 */
public class LiveChatActivity extends Activity {
    private ImageButton mBackToMyTrainerImgBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.live_chat);

        mBackToMyTrainerImgBtn = (ImageButton) findViewById(R.id.live_chat_back_img);
        mBackToMyTrainerImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
