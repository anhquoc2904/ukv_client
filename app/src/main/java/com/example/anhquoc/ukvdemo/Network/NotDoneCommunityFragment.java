package com.example.anhquoc.ukvdemo.Network;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.anhquoc.ukvdemo.R;
import com.example.anhquoc.ukvdemo.Setting.SettingFragment;

/**
 * Created by Anh Quoc on 2/27/2017.
 */
public class NotDoneCommunityFragment extends Fragment {
    private ImageButton mBackImgBtn;
    private TextView mMenuName;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.not_done_ukv, container, false);

        String menuName=getArguments().getString("MenuName");

        mBackImgBtn = (ImageButton) view.findViewById(R.id.not_done_back_img);
        mMenuName = (TextView) view.findViewById(R.id.title_name);
        if (menuName!=null) {
            mMenuName.setText(menuName);
        }
        mBackImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new CommunityFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });
        return view;
    }
}
