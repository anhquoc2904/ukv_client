package com.example.anhquoc.ukvdemo.Network;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.MainActivity;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/18/2016.
 */
public class UserChatFragment extends Fragment {
    ImageButton mBackToDrawerImgBtn;
    Button mCandidateBtn;
    Button mJoinGroupBtn;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_chat_fragment, container, false);

        mBackToDrawerImgBtn = (ImageButton) view.findViewById(R.id.user_chat_back_img);
        mCandidateBtn = (Button) view.findViewById(R.id.user_chat_btn);
        mJoinGroupBtn = (Button) view.findViewById(R.id.new_group_btn);

        mBackToDrawerImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).openDrawerFromFragment();
            }
        });

        mCandidateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NetworkChatActivity.class);
                startActivity(intent);
            }
        });

        mJoinGroupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), GroupChatActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
