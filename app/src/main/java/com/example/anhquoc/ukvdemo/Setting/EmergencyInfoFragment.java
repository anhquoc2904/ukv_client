package com.example.anhquoc.ukvdemo.Setting;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anhquoc.ukvdemo.R;

import java.io.IOException;

/**
 * Created by Anh Quoc on 10/18/2016.
 */

public class EmergencyInfoFragment extends Fragment {
    private int PICK_IMAGE_REQUEST = 1;
    ImageButton mBackToMyHealthImgBtn;
    TextView mInsuranceNumberTxt;
    ImageView mInsuranceNumberEdit;
    TextView mNameTxt;
    ImageView mNameEdit;
    TextView mAddress1Txt;
    ImageView mAddress1Edit;
    TextView mAddress2Txt;
    ImageView mAddress2Edit;
    TextView mPhoneNumberTxt;
    ImageView mPhoneNumberEdit;
    TextView mEmailTxt;
    ImageView mEmailEdit;
    TextView mBloodGroupTxt;
    ImageView mBloodGroupEdit;
    TextView mDiseaseTxt;
    ImageView mDiseaseEdit;
    TextView mAllergyTxt;
    ImageView mAllergyEdit;
    TextView mIntoleranceTxt;
    ImageView mIntoleranceEdit;

    ImageView mProfilePhoto;
    Uri mProfilePhotoUri;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.emergency_data_layout, container, false);
        mBackToMyHealthImgBtn = (ImageButton) view.findViewById(R.id.emergency_info_back_img);
        mInsuranceNumberTxt = (TextView) view.findViewById(R.id.insurance_number_txt);
        mNameTxt = (TextView) view.findViewById(R.id.name);
        mPhoneNumberTxt = (TextView) view.findViewById(R.id.phone_number);
        mAddress1Txt = (TextView) view.findViewById(R.id.address_1);
        mAddress2Txt = (TextView) view.findViewById(R.id.address_2);
        mEmailTxt = (TextView) view.findViewById(R.id.email);
        mBloodGroupTxt = (TextView) view.findViewById(R.id.blood_group_txt);
        mDiseaseTxt = (TextView) view.findViewById(R.id.disease_txt);
        mAllergyTxt = (TextView) view.findViewById(R.id.allergy_txt);
        mIntoleranceTxt = (TextView) view.findViewById(R.id.intolerance_txt);

        mInsuranceNumberEdit = (ImageView) view.findViewById(R.id.edit_insurance_number_btn);
        mNameEdit = (ImageView) view.findViewById(R.id.edit_name_btn);
        mAddress1Edit = (ImageView) view.findViewById(R.id.edit_address1_btn);
        mAddress2Edit = (ImageView) view.findViewById(R.id.edit_address2_btn);
        mPhoneNumberEdit = (ImageView) view.findViewById(R.id.edit_phone_number_btn);
        mEmailEdit = (ImageView) view.findViewById(R.id.edit_email_btn);
        mBloodGroupEdit = (ImageView) view.findViewById(R.id.edit_blood_group);
        mDiseaseEdit = (ImageView) view.findViewById(R.id.edit_disease);
        mAllergyEdit = (ImageView) view.findViewById(R.id.edit_allergy);
        mIntoleranceEdit = (ImageView) view.findViewById(R.id.edit_intolerance);

        mProfilePhoto = (ImageView) view.findViewById(R.id.photo);

        ReadFromSharedPreferences();

        mProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        mBackToMyHealthImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SettingFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mInsuranceNumberEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_info_insurance_number));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mInsuranceNumberTxt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        mNameEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_personal_info));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mNameTxt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        mAddress1Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_personal_info));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mAddress1Txt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        mAddress2Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_personal_info));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mAddress2Txt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        mPhoneNumberEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_personal_info));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_PHONE );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mPhoneNumberTxt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        mEmailEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_personal_info));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mEmailTxt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        mBloodGroupEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_emergency_info));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mBloodGroupTxt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        mDiseaseEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_emergency_info));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mDiseaseTxt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        mAllergyEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_emergency_info));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mAllergyTxt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        mIntoleranceEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                builder.setTitle(getString(R.string.my_health_emergency_info));

                // Set up the input
                final EditText input = new EditText(getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString()!=null) {
                            mIntoleranceTxt.setText(input.getText().toString());
                        }
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        imm.toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS,0);
                    }
                });

                builder.show();
            }
        });

        return  view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            mProfilePhotoUri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mProfilePhotoUri);
                // Log.d(TAG, String.valueOf(bitmap));

                mProfilePhoto.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        SaveToSharedPreferences();
    }

    private void SaveToSharedPreferences() {
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Insurance number", mInsuranceNumberTxt.getText().toString());
        editor.putString("Name", mNameTxt.getText().toString());
        editor.putString("Address1", mAddress1Txt.getText().toString());
        editor.putString("Address2", mAddress2Txt.getText().toString());
        editor.putString("Phone Number", mPhoneNumberTxt.getText().toString());
        editor.putString("Email", mEmailTxt.getText().toString());
        editor.putString("Blood Group", mBloodGroupTxt.getText().toString());
        editor.putString("Disease", mDiseaseTxt.getText().toString());
        editor.putString("Allergy", mAllergyTxt.getText().toString());
        editor.putString("Intolerance", mIntoleranceTxt.getText().toString());
        if (mProfilePhotoUri != null) {
            editor.putString("ProfilePhotoUri", mProfilePhotoUri.toString());
        }
        editor.commit();
    }

    private void ReadFromSharedPreferences(){
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        String InsuranceNumber = sharedPref.getString("Insurance number", "MNCV-23561-846");
        mInsuranceNumberTxt.setText(InsuranceNumber);

        String Name = sharedPref.getString("Name", "Max Mustermann");
        mNameTxt.setText(Name);

        String Address1 = sharedPref.getString("Address1", "Musterstr.24");
        mAddress1Txt.setText(Address1);

        String Address2 = sharedPref.getString("Address2", "12345 Musterhausen");
        mAddress2Txt.setText(Address2);

        String PhoneNumber = sharedPref.getString("Phone Number", "017626168100");
        mPhoneNumberTxt.setText(PhoneNumber);

        String Email = sharedPref.getString("Email", "mustermann@web.de");
        mEmailTxt.setText(Email);

        String BloodGroup = sharedPref.getString("Blood Group", "0 negativ");
        mBloodGroupTxt.setText(BloodGroup);

        String Disease = sharedPref.getString("Disease", "Diabetes II");
        mDiseaseTxt.setText(Disease);

        String Allergy = sharedPref.getString("Allergy", "Herzinsuffizient");
        mAllergyTxt.setText(Allergy);

        String Intolerance = sharedPref.getString("Intolerance", "Laktose");
        mIntoleranceTxt.setText(Intolerance);

        String UriString = sharedPref.getString("ProfilePhotoUri", "nulluri");
        if (UriString != "nulluri") {
            Uri uri = Uri.parse(UriString);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                mProfilePhoto.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
