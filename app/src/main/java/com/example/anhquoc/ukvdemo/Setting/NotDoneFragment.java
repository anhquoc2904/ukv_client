package com.example.anhquoc.ukvdemo.Setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.anhquoc.ukvdemo.Apply.AfterFilterApplyFragment;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 1/11/2017.
 */
public class NotDoneFragment extends Fragment {

    private ImageButton mBackImgBtn;
    private TextView mMenuName;
    @Nullable
    @Override
    public View  onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.not_done_ukv, container, false);

//        fragment.getView().setFocusableInTouchMode(true);
//        fragment.getView().requestFocus();
//        fragment.getView().setOnKeyListener( new View.OnKeyListener()
//        {
//            @Override
//            public boolean onKey( View v, int keyCode, KeyEvent event )
//            {
//                if( keyCode == KeyEvent.KEYCODE_BACK )
//                {
//                    Fragment fragment = new SettingFragment();
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.fragment_layout, fragment)
//                            .commit();
//                    return true;
//                }
//                return false;
//            }
//        } );
        String menuName=getArguments().getString("MenuName");

        mBackImgBtn = (ImageButton) view.findViewById(R.id.not_done_back_img);
        mMenuName = (TextView) view.findViewById(R.id.title_name);
        if (menuName!=null) {
            mMenuName.setText(menuName);
        }
        mBackImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SettingFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });
        return view;
    }

}
