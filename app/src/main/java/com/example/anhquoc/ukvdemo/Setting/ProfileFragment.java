package com.example.anhquoc.ukvdemo.Setting;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anhquoc.ukvdemo.MainActivity;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 2/26/2017.
 */
public class ProfileFragment extends Fragment {
    private ImageButton mBackImgBtn;
    private Button mSaveButton;
    private Spinner mJobsSpinner;
    private Spinner mFamilyStatusSpinner;
    private Spinner mStatesSpinner;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.profile_fragment, container, false);

        mJobsSpinner = (Spinner) view.findViewById(R.id.jobs_spinner);
        mFamilyStatusSpinner = (Spinner) view.findViewById(R.id.family_status_spinner);
        mStatesSpinner = (Spinner) view.findViewById(R.id.states_spinner);

        mBackImgBtn = (ImageButton) view.findViewById(R.id.back_img);
        mSaveButton = (Button) view.findViewById(R.id.save_profile);

        mBackImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SettingFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mJobsSpinner.getSelectedItemPosition() == 0
                        || mFamilyStatusSpinner.getSelectedItemPosition() == 0
                        || mStatesSpinner.getSelectedItemPosition() == 0) {
                    Toast toast = Toast.makeText(getContext(), "Please answer all the question to continue", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                SaveToSharedPreferences();
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.setProfileInDrawerFromSharedPreferences();
                Toast toast = Toast.makeText(getContext(), "New profile is saved", Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> jobs_adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.jobs_array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        jobs_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // Apply the adapter to the spinner
        mJobsSpinner.setAdapter(jobs_adapter);

        ArrayAdapter<CharSequence> family_status_adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.family_status_array, R.layout.spinner_item);
        family_status_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mFamilyStatusSpinner.setAdapter(family_status_adapter);

        ArrayAdapter<CharSequence> states_adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.states_array, R.layout.spinner_item);
        states_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mStatesSpinner.setAdapter(states_adapter);

        ReadFromSharedPreferences();
        return view;
    }

    private void SaveToSharedPreferences() {
        SharedPreferences sharedPref = getActivity().getSharedPreferences("UserProfile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("User Job", mJobsSpinner.getSelectedItem().toString());
        editor.putInt("User Job Position", mJobsSpinner.getSelectedItemPosition());

        editor.putString("User Family Status", mFamilyStatusSpinner.getSelectedItem().toString());
        editor.putInt("User Family Status Position", mFamilyStatusSpinner.getSelectedItemPosition());

        editor.putString("User State", mStatesSpinner.getSelectedItem().toString());
        editor.putInt("User State Position", mStatesSpinner.getSelectedItemPosition());

        editor.commit();

    }

    private void ReadFromSharedPreferences() {
        SharedPreferences sharedPref = getActivity().getSharedPreferences("UserProfile", Context.MODE_PRIVATE);
        mJobsSpinner.setSelection(sharedPref.getInt("User Job Position", 0));
        mFamilyStatusSpinner.setSelection(sharedPref.getInt("User Family Status Position", 0));
        mStatesSpinner.setSelection(sharedPref.getInt("User State Position", 0));
    }
}
