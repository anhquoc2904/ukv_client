package com.example.anhquoc.ukvdemo.Sport;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.anhquoc.ukvdemo.Network.LiveChatActivity;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/18/2016.
 */
public class FitImActivity extends Activity {
    ImageButton mBackToSportCheckImgBtn;
    ImageButton mLiveChatImgBtn;
    Button mAnalogyTest;
    Button mConclusionTest;
    Button mMatrixTest;
    Button mNumberTest;
    Button mEquationTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fit_im);
        mBackToSportCheckImgBtn = (ImageButton) findViewById(R.id.fit_im_back_img);
        mLiveChatImgBtn = (ImageButton) findViewById(R.id.live_chat_img);
        mAnalogyTest = (Button) findViewById(R.id.analogies_test);
        mConclusionTest = (Button) findViewById(R.id.conclusion_test);
        mMatrixTest = (Button) findViewById(R.id.matrix_test);
        mNumberTest = (Button) findViewById(R.id.number_test);
        mEquationTest = (Button) findViewById(R.id.equation_test);
        mBackToSportCheckImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mLiveChatImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FitImActivity.this, LiveChatActivity.class);
                startActivity(intent);
            }
        });

        mAnalogyTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FitImActivity.this, "Analogien ist noch nicht fertig", Toast.LENGTH_SHORT).show();
            }
        });

        mConclusionTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FitImActivity.this, "Schlussfolgerungen ist noch nicht fertig", Toast.LENGTH_SHORT).show();
            }
        });

        mMatrixTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FitImActivity.this, "Matrizen ist noch nicht fertig", Toast.LENGTH_SHORT).show();
            }
        });

        mNumberTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FitImActivity.this, "Zahlenreihen ist noch nicht fertig", Toast.LENGTH_SHORT).show();
            }
        });

        mEquationTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FitImActivity.this, "Gleichungen ist noch nicht fertig", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
