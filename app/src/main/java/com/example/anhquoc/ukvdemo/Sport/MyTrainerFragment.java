package com.example.anhquoc.ukvdemo.Sport;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.anhquoc.ukvdemo.Network.LiveChatActivity;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/17/2016.
 */
public class MyTrainerFragment extends Fragment {

    private ImageButton mBackToSportCheckImgBtn;
    private ImageButton mLiveChatImgBtn;
    private ImageView mFitnessCourseImg;
    private ImageView mFitnessStudioImg;
    private ImageView mSwimmingImg;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_trainer, container, false);
        mBackToSportCheckImgBtn = (ImageButton) view.findViewById(R.id.my_trainer_back_img);
        mLiveChatImgBtn = (ImageButton) view.findViewById(R.id.live_chat_img);
        mFitnessCourseImg = (ImageView) view.findViewById(R.id.fitness_course);
        mFitnessStudioImg = (ImageView) view.findViewById(R.id.fitness_studio);
        mSwimmingImg = (ImageView) view.findViewById(R.id.swimming);

        mBackToSportCheckImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SportFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });
        mLiveChatImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LiveChatActivity.class);
                startActivity(intent);
            }
        });

        mFitnessCourseImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Mein Fitness-Parcours", Toast.LENGTH_SHORT).show();
                Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.example.zhoubo.interactivegui");
                if (launchIntent != null) {
                    startActivity(launchIntent);//null pointer check in case package name was not found
                }
            }
        });

        mFitnessStudioImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Mein Fitnesstudio", Toast.LENGTH_SHORT).show();
                Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.example.zhoubo.interactivegui");
                if (launchIntent != null) {
                    startActivity(launchIntent);//null pointer check in case package name was not found
                }
            }
        });

        mSwimmingImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Schwimmen", Toast.LENGTH_SHORT).show();
                Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage("com.example.zhoubo.interactivegui");
                if (launchIntent != null) {
                    startActivity(launchIntent);//null pointer check in case package name was not found
                }
            }
        });

        return view;
    }
}
