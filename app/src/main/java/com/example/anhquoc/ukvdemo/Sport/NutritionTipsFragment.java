package com.example.anhquoc.ukvdemo.Sport;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anhquoc.ukvdemo.Network.LiveChatActivity;
import com.example.anhquoc.ukvdemo.OnSwipeTouchListener;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/18/2016.
 */
public class NutritionTipsFragment extends Fragment {
    private ImageButton mBackNutritionImgBtn;
    private ImageButton mNextNutritionImgBtn;
    private ImageButton mLiveChatImgBtn;
    private TextView mNutritionTitleTxt;
    private TextView mNutritionNameTxt;
    private ImageView mNutritionImage;
    private int mMapNumber = 1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.nutrition_tips, container, false);
        mBackNutritionImgBtn = (ImageButton) view.findViewById(R.id.nutrition_back_img);
        mNextNutritionImgBtn = (ImageButton) view.findViewById(R.id.nutrition_next_img);
        mLiveChatImgBtn = (ImageButton) view.findViewById(R.id.live_chat);
        mNutritionTitleTxt = (TextView) view.findViewById(R.id.nutrition_title);
        mNutritionNameTxt = (TextView) view.findViewById(R.id.nutrition_name);
        mNutritionImage = (ImageView) view.findViewById(R.id.nutrition_img);

        mBackNutritionImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMapNumber == 1) {
                    Fragment fragment = new SportFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, fragment)
                            .commit();
                }
                else if (mMapNumber == 2) {
                    mMapNumber --;
                    mNextNutritionImgBtn.setVisibility(View.VISIBLE);
                    mNutritionTitleTxt.setText(getResources().getString(R.string.my_health_nutrition_tips));
                    mNutritionNameTxt.setText(getResources().getString(R.string.my_health_nutrition_tips_recommendation));
                    mNutritionImage.setImageResource(R.drawable.nutrition_recommendation);
                }
            }
        });

        mNextNutritionImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMapNumber ++;
                mNextNutritionImgBtn.setVisibility(View.INVISIBLE);
                mNutritionTitleTxt.setText(getResources().getString(R.string.my_health_nutrition_tips_recipe));
                mNutritionNameTxt.setText(getResources().getString(R.string.my_health_nutrition_tips_fast_recipe));
                mNutritionImage.setImageResource(R.drawable.recipe);
            }
        });
        mNutritionImage.setOnTouchListener(new OnSwipeTouchListener(getContext()) {
            public void onSwipeTop() {
            }
            public void onSwipeRight() {
                if (mMapNumber == 1) {
                    Fragment fragment = new SportFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_layout, fragment)
                            .commit();
                }
                else if (mMapNumber == 2) {
                    mMapNumber --;
                    mNextNutritionImgBtn.setVisibility(View.VISIBLE);
                    mNutritionTitleTxt.setText(getResources().getString(R.string.my_health_nutrition_tips));
                    mNutritionNameTxt.setText(getResources().getString(R.string.my_health_nutrition_tips_recommendation));
                    mNutritionImage.setImageResource(R.drawable.nutrition_recommendation);
                }
            }
            public void onSwipeLeft() {
                if (mMapNumber<2) {
                    mMapNumber++;
                }
                mNextNutritionImgBtn.setVisibility(View.INVISIBLE);
                mNutritionTitleTxt.setText(getResources().getString(R.string.my_health_nutrition_tips_recipe));
                mNutritionNameTxt.setText(getResources().getString(R.string.my_health_nutrition_tips_fast_recipe));
                mNutritionImage.setImageResource(R.drawable.recipe);
            }
            public void onSwipeBottom() {
            }

        });

        mLiveChatImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LiveChatActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }
}
