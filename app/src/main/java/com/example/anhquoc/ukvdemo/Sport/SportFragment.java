package com.example.anhquoc.ukvdemo.Sport;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.anhquoc.ukvdemo.MainActivity;
import com.example.anhquoc.ukvdemo.MapsActivity;
import com.example.anhquoc.ukvdemo.R;

/**
 * Created by Anh Quoc on 10/14/2016.
 */
public class SportFragment extends Fragment {
    private Button mSportToDoBtn;
    private Button mChecklistBtn;
    private Button mFilmBtn;
    private Button mSimpleTrackingBtn;
    private Button mMapBtn;
    private Button mNutritionBtn;
    private ImageButton mBackToDrawerImgBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sport_fragment, container, false);
        mSportToDoBtn = (Button) view.findViewById(R.id.sport_todos_btn);
        mChecklistBtn = (Button) view.findViewById(R.id.checklist_btn);
        mFilmBtn = (Button) view.findViewById(R.id.film_btn);
        mSimpleTrackingBtn = (Button) view.findViewById(R.id.tracking_btn);
        mMapBtn = (Button) view.findViewById(R.id.map_btn);
        mNutritionBtn = (Button) view.findViewById(R.id.nutrition_btn);

        mBackToDrawerImgBtn = (ImageButton) view.findViewById(R.id.sport_check_back_img);

        mBackToDrawerImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).openDrawerFromFragment();
            }
        });

        mSportToDoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "ToDos");
                Fragment fragment = new NotDoneSportFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mChecklistBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Checklisten");
                Fragment fragment = new NotDoneSportFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });


        mFilmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("MenuName", "Erklär-Filme");
                Fragment fragment = new NotDoneSportFragment();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mSimpleTrackingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new MyTrainerFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });

        mMapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                startActivity(intent);
            }
        });

        mNutritionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new NutritionTipsFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fragment)
                        .commit();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
