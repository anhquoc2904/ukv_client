package com.example.anhquoc.ukvdemo.Start;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.anhquoc.ukvdemo.MainActivity;
import com.example.anhquoc.ukvdemo.R;

import java.io.IOException;

/**
 * Created by Anh Quoc on 2/22/2017.
 */
public class AskInformationActivity extends AppCompatActivity {
    private Button mInputDoneBtn;
    private Context mContext;
    private Activity mActivity;
    private Spinner mJobsSpinner;
    private Spinner mFamilyStatusSpinner;
    private Spinner mStatesSpinner;
    private Boolean mDontShowAgainIsChecked = false;


    private CheckBox mDontShowAgain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        mContext = this;
        mActivity = this;
        if (UserProfileIsReady()) {
            Intent intent = new Intent(mContext, MainActivity.class);
            startActivity(intent);
        }
        setContentView(R.layout.ask_information);


        mJobsSpinner = (Spinner) findViewById(R.id.jobs_spinner);
        mFamilyStatusSpinner = (Spinner) findViewById(R.id.family_status_spinner);
        mStatesSpinner = (Spinner) findViewById(R.id.states_spinner);
        mDontShowAgain = (CheckBox) findViewById(R.id.dont_show_again);
        mInputDoneBtn = (Button) findViewById(R.id.input_done);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.jobs_array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // Apply the adapter to the spinner
        mJobsSpinner.setAdapter(adapter);

        mJobsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> jobs_adapter = ArrayAdapter.createFromResource(this,
                R.array.jobs_array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        jobs_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // Apply the adapter to the spinner
        mJobsSpinner.setAdapter(jobs_adapter);

        mJobsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<CharSequence> family_status_adapter = ArrayAdapter.createFromResource(this,
                R.array.family_status_array, R.layout.spinner_item);
        family_status_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mFamilyStatusSpinner.setAdapter(family_status_adapter);

        mFamilyStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> states_adapter = ArrayAdapter.createFromResource(this,
                R.array.states_array, R.layout.spinner_item);
        states_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mStatesSpinner.setAdapter(states_adapter);

        mStatesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ReadFromSharedPreferences();
        mInputDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mJobsSpinner.getSelectedItemPosition() == 0
                        || mFamilyStatusSpinner.getSelectedItemPosition() == 0
                        || mStatesSpinner.getSelectedItemPosition() == 0) {
                    Toast toast = Toast.makeText(mContext, "Please answer all the question to continue", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }

                    if (mDontShowAgain.isChecked()) {
                        mDontShowAgainIsChecked = true;
                    } else {
                        mDontShowAgainIsChecked = false;
                    }
                SaveToSharedPreferences();
                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(intent);
            }
        });
        super.onCreate(savedInstanceState);
    }

    private void SaveToSharedPreferences() {
        SharedPreferences sharedPref = mActivity.getSharedPreferences("UserProfile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("User Job", mJobsSpinner.getSelectedItem().toString());
        editor.putInt("User Job Position", mJobsSpinner.getSelectedItemPosition());

        editor.putString("User Family Status", mFamilyStatusSpinner.getSelectedItem().toString());
        editor.putInt("User Family Status Position", mFamilyStatusSpinner.getSelectedItemPosition());

        editor.putString("User State", mStatesSpinner.getSelectedItem().toString());
        editor.putInt("User State Position", mStatesSpinner.getSelectedItemPosition());

        editor.putBoolean("Dont Show Again", mDontShowAgainIsChecked);

        editor.commit();

    }

    private void ReadFromSharedPreferences() {
        SharedPreferences sharedPref = mActivity.getSharedPreferences("UserProfile", Context.MODE_PRIVATE);
        mJobsSpinner.setSelection(sharedPref.getInt("User Job Position", 0));
        mFamilyStatusSpinner.setSelection(sharedPref.getInt("User Family Status Position", 0));
        mStatesSpinner.setSelection(sharedPref.getInt("User State Position", 0));
    }

    private boolean UserProfileIsReady() {
        SharedPreferences sharedPref = mActivity.getSharedPreferences("UserProfile", Context.MODE_PRIVATE);

        Boolean start = sharedPref.getBoolean("Dont Show Again", false);
        return start;
    }


}
